 var app = angular.module('myApp', ['ngGrid', 'ngDialog', 'ngFileUpload'])

 .factory('GridService1', ['$http', '$q',
     function($http, $q) {
         var contributorsFile = 'json/new-payout-cards.json';
         var contributors = [];

         function getContributors() {
             var deferred = $q.defer();
             $http.get(contributorsFile)
                 .then(function(result) {
                     contributors = result.data;
                     deferred.resolve(contributors);
                 }, function(error) {
                     deferred.reject(error);
                 });
             return deferred.promise;
         }
         return {
             getContributors: getContributors
         };
     }
 ])

 app.controller('MyCtrl3', ['$scope', 'GridService1', 'ngDialog', 'Upload',
     function($scope, GridService1, ngDialog, Upload) {
         GridService1.getContributors().then(function(data) {
             $scope.myData = data;
         });

         $scope.gridOptions1 = {
             data: 'myData',
             jqueryUITheme: true,
             enablePinning: false,
             enablePaging: true,
             showFooter: true,
             headerRowHeight: 40,
             totalServerItems: 'totalServerItems',
             pagingOptions: {
                 pageSizes: [15, 30, 60],
                 pageSize: 15,
                 totalServerItems: 0,
                 currentPage: 1
             },
             //filterOptions: $scope.filterOptions,
             columnDefs: [{
                 field: 'Date',
                 displayName: 'Date'
             }, {
                 field: 'Action',
                 displayName: 'Action'
             }, {
                 field: 'Transferred_to',
                 displayName: 'Transferred to'
             }, {
                 field: 'Amount',
                 displayName: 'Amount'
             }],
             headerRowTemplate: '../headerRowTemplate.html',
             showFooter: false
                 // showSelectionCheckbox: true,
                 // checkboxCellTemplate: '<div class="ngSelectionCell"><input tabindex="-1" class="ngSelectionCheckbox" type="checkbox" ng-checked="row.selected" /> Some text here</div>',

         };

         $scope.lockCard = function () {
            ngDialog.open({ 
                template: 'lock-card-modal.html',
                className: 'ngdialog-theme-default',
                scope: $scope
             });
        };

     }

 ]);
