jQuery(function($) {

    $("#menu-toggle").on('touchstart click', function (e) {
        e.preventDefault();
        $(".desktop-detected").toggleClass("toggled");
    });

    var lefMenu = function() {
        if ($(window).width() >= 768) {
           $(".desktop-detected").removeClass("toggled");
        }
    };
    $(window).on('resize load', lefMenu);


    htF = function() {
        var fooHeight = window.innerHeight;
        var imgFooHeight = fooHeight - $(".js-footer__ht").outerHeight() - $(".js-header__ht").outerHeight();
        console.log(fooHeight, $(".js-footer__ht").outerHeight(), $(".js-header__ht").outerHeight());
        $('.js-minHt').css("min-height", imgFooHeight);
        var screenWidth = $(window).width();
        var screenHeight = $(window).height();
        if ( (screenWidth >= 1024) && (screenHeight >= 768)) {
            $('.js-Ht').css({"height": imgFooHeight, "overflow": "auto"});
            $('html').css({"overflow": "hidden"});
        } else {
            $('.js-Ht').css({"height": "auto", "overflow": "visible"});
            $('html').css({"overflow": "auto"});
        }
    };
    $(window).on('resize load', htF);


    // prof-edit
    $('.js-btn-data-edit').on("click", function() {
        $(this).parents(".js-editable-form-group").stop().removeClass("inactive");
        $(this).parents(".js-editable-form-group").find('.form-control').prop("disabled", false);
    });

    $('.js-btn-data-save').on("click", function() {
        $(this).parents(".js-editable-form-group").stop().addClass("inactive");
        $(this).parents(".js-editable-form-group").find('.form-control').prop("disabled", "disabled");
    });



    $(document).on(
        'click.bs.dropdown.data-api', 
        '[data-toggle="no-collapse"]',
        function (e) { e.stopPropagation(); }
    );

    $('.js-btn-edit-input').on('click tup', function() {
        $('.js-form-group-edit').toggleClass('form-group-edit');
        $(".js-input-ds").prop('disabled', function () {
           return ! $(this).prop('disabled');
        });
        if ($(this).children('.icon').hasClass("i-edit-i")) {
            $(this).children('.icon').removeClass("i-edit-i");
            $(this).children('.icon').addClass("i-check");
        } else {
            $(this).children('.icon').removeClass("i-check");
            $(this).children('.icon').addClass("i-edit-i");
        }
    });

    $(function() {

      jcf.setOptions('Select', {
          maxVisibleItems: 7
      });

      jcf.replaceAll();
    });
});


//Check to see if placeholder is supported
//Hat tip: http://diveintohtml5.info/detect.html#input-placeholder
if (!Modernizr.input.placeholder) {

    function supports_input_placeholder() {
        var i = document.createElement('input');
        return 'placeholder' in i;
    }

    if (supports_input_placeholder()) {
        // The placeholder attribute is supported
        //Do nothing. Smile. Go outside
    } else {
        // The "placeholder" attribute isn't supported. Let's fake it.

        var $placeholder = $(":input[placeholder]"); // Get all the input elements with the "placeholder" attribute

        // Go through each element and assign the "value" attribute the value of the "placeholder"
        // This will allow users with no support to see the default text
        $placeholder.each(function() {
            var $message = $(this).attr("placeholder");
            $(this).attr("value", $message);
        });

        // When a user clicks the input (on focus) the default text will be removed
        $placeholder.focus(function() {
            var $value = $(this).attr("value");
            var $placeholderTxt = $(this).attr("placeholder");

            if ($value == $placeholderTxt) {
                $(this).attr("value", "");
            }
        });

        // When a user clicks/tabs away from the input (on blur) keep what they typed
        // Unless they didn't type anything, in that case put back in the default text
        $placeholder.blur(function() {
            var $value = $(this).attr("value");
            var $placeholderTxt = $(this).attr("placeholder");

            if ($value == '') {
                $(this).attr("value", $placeholderTxt);
            }
        });

        // Since we're inputing text into the "value" attribute we need to make 
        // sure that this default text isn't submitted with the form, potentially
        // causing validation issues. So we're going to remove the default text
        // and submit the inputs as blank.
        $("form").submit(function() {
            var $checkValue = $(":input");

            $checkValue.each(function() {
                if ($(this).attr("value") == $(this).attr("placeholder")) {
                    $(this).attr("value", "");
                }
            });
        });
    }
}
