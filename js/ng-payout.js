 var app = angular.module('myApp', ['ngGrid', 'ngDialog'])

 .factory('GridService2', ['$http', '$q',
     function($http, $q) {
         var contributorsFile = 'json/payout.json';
         var contributors = [];
         function getContributors() {
             var deferred = $q.defer();
             $http.get(contributorsFile)
                 .then(function(result) {
                     contributors = result.data;
                     deferred.resolve(contributors);
                 }, function(error) {
                     deferred.reject(error);
                 });
             return deferred.promise;
         }
         return {
             getContributors: getContributors
         };
     }
 ])

app.controller('MyCtrl2', ['$scope', 'GridService2', 'ngDialog',
     function($scope, GridService2, ngDialog) {
         GridService2.getContributors().then(function(data) {
             $scope.myData = data;
         });

         $scope.gridOptions = {
             data: 'myData',
             jqueryUITheme: true,
             enablePinning: false,
             enablePaging: true,
             showFooter: true,
             headerRowHeight: 40,
             totalServerItems: 'totalServerItems',
             pagingOptions: {
                 pageSizes: [15, 30, 60],
                 pageSize: 15,
                 totalServerItems: 0,
                 currentPage: 1
             },
             //filterOptions: $scope.filterOptions,
             columnDefs: [
                 {field:'Data', displayName: 'Data'},
                 {field:'Name', displayName:'Name'},
                 {field:'Reference'},
                 {field:'Data_2', displayName:'Date'},
                 {field:'Mempers'},
                 {field:'Amount'},
                 {field:'Stauts'}
             ],
             headerRowTemplate: '../headerRowTemplate.html',
             footerTemplate: '../footerTemplate.html'
         };


        $scope.clickToOpen = function () {
            ngDialog.open({ 
                template: 'new-payout-modal.html',
                className: 'ngdialog-theme-default ng-modal-lg'
             });
        };
     }

]);

