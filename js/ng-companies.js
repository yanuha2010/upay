var app = angular.module('myApp', ['ngDialog', 'ngFileUpload']);

app.controller('MyCtrl2', ['$scope', 'ngDialog', 'Upload',
     function($scope, ngDialog, Upload) {

        $scope.joinCompany = function () {
            ngDialog.open({ 
                template: 'join-company-modal.html',
                className: 'ngdialog-theme-default ng-modal-lg',
                scope: $scope
             });
        };

        $scope.joinCompany2 = function () {
            ngDialog.open({ 
                template: 'join-company-modal-2.html',
                className: 'ngdialog-theme-default ng-modal-lg',
                scope: $scope
             });
        };

        $scope.joinCompany3 = function () {
            ngDialog.open({ 
                template: 'join-company-modal-3.html',
                className: 'ngdialog-theme-default ng-modal',
                scope: $scope
             });
        };

        $scope.joinCompany4 = function () {
            ngDialog.open({ 
                template: 'join-company-modal-4.html',
                className: 'ngdialog-theme-default ng-modal',
                scope: $scope
             });
        };

        $scope.checkCard = function () {
            ngDialog.open({ 
                template: 'check-card-modal.html',
                className: 'ngdialog-theme-default ng-modal',
                scope: $scope
             });
        };
        
        $scope.sendTheCode = function () {
            ngDialog.open({ 
                template: 'send-the-code-modal.html',
                className: 'ngdialog-theme-default ng-modal',
                scope: $scope
             });
        };

        $scope.sendTheCode2 = function () {
            ngDialog.open({ 
                template: 'send-the-code-2-modal.html',
                className: 'ngdialog-theme-default ng-modal-lg',
                scope: $scope
             });
        };

        // $scope.$watch('files', function () {
        //     $scope.upload($scope.files);
        // });
        // $scope.log = '';

        // $scope.upload = function (files) {
        //     if (files && files.length) {
        //         for (var i = 0; i < files.length; i++) {
        //             var file = files[i];
        //             Upload.upload({
        //                 url: 'https://angular-file-upload-cors-srv.appspot.com/upload',
        //                 fields: {
        //                     'username': $scope.username
        //                 },
        //                 file: file
        //             }).progress(function (evt) {
        //                 var progressPercentage = parseInt(100.0 * evt.loaded / evt.total);
        //                 $scope.log = 'progress: ' + progressPercentage + '% ' +
        //                             evt.config.file.name + '\n' + $scope.log;
        //             }).success(function (data, status, headers, config) {
        //                 $timeout(function() {
        //                     $scope.log = 'file: ' + config.file.name + ', Response: ' + JSON.stringify(data) + '\n' + $scope.log;
        //                 });
        //             });
        //         }
        //     }
        // };

        $scope.$watch('files', function() {
             $scope.upload($scope.files);
         });

         $scope.upload = function(files) {
             if (files && files.length) {
                 for (var i = 0; i < files.length; i++) {
                     var file = files[i];
                     $scope.file_ = file;
                     Upload.upload({
                         url: 'https://angular-file-upload-cors-srv.appspot.com/upload',
                         file: file
                     });
                 }
             }
         };

     }

]);


(function () {
  var myApp = angular.module('myApp', ['angular-bootstrap-select']);

  myApp.controller('MyCtrl2', function ($scope, $timeout) {
    $scope.model = '';
    $scope.colors = ['Mustard', 'Ketchup', 'Relish'];
    $scope.repeater = [
      { title: 'one' },
      { title: 'two' },
      { title: 'three' }
    ];
    $scope.selectWithOptionsIsVisible = true;
  });
  
  
  /**
   * inject angular-bootstrap-select
   * https://raw.githubusercontent.com/joaoneto/angular-bootstrap-select/v0.0.4/src/angular-bootstrap-select.js
   */

  angular.module('angular-bootstrap-select', [])
  .directive('selectpicker', ['$parse', function ($parse) {
    return {
      restrict: 'A',
      link: function (scope, element, attrs) {
        element.selectpicker($parse(attrs.selectpicker)());
        element.selectpicker('refresh');
        
        scope.$watch(attrs.ngModel, function (newVal, oldVal) {
          scope.$parent[attrs.ngModel] = newVal;
          scope.$evalAsync(function () {
            if (!attrs.ngOptions || /track by/.test(attrs.ngOptions)) element.val(newVal);
            element.selectpicker('refresh');
          });
        });
        
        scope.$on('$destroy', function () {
          scope.$evalAsync(function () {
            element.selectpicker('destroy');
          });
        });
      }
    };
  }]);
})();
