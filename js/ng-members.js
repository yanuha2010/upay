 var app = angular.module('myApp', ['ngGrid', 'ngTagsInput'])

   .factory('GridService', ['$http', '$q',
       function($http, $q) {
           var contributorsFile = 'json/members.json';
           var contributors = [];
           function getContributors() {
               var deferred = $q.defer();
               $http.get(contributorsFile)
                   .then(function(result) {
                       contributors = result.data;
                       deferred.resolve(contributors);
                   }, function(error) {
                       deferred.reject(error);
                   });
               return deferred.promise;
           }
           return {
               getContributors: getContributors
           };
       }
   ])

   .controller('MyCtrl', ['$scope', 'GridService',
       function($scope, gridService) {
           gridService.getContributors().then(function(data) {
               $scope.myData = data;
           });

           $scope.gridOptions = {
               data: 'myData',
               jqueryUITheme: true,
               enablePinning: false,
               enablePaging: true,
               showFooter: true,
               headerRowHeight: 40,
               totalServerItems: 'totalServerItems',
               pagingOptions: {
                   pageSizes: [15, 30, 60],
                   pageSize: 15,
                   totalServerItems: 0,
                   currentPage: 1
               },
               //filterOptions: $scope.filterOptions,
               columnDefs: [
                   {field:'User_status', displayName: ' ', width: 40, headerClass: 'ng-user-status__header', cellClass: 'ng-user-status__wr', cellTemplate: '<div class="ngCellText"><span class="user-status {{row.entity[col.field]}}"></span></div>'},
                   {field:'ID_Namber', displayName:'ID Namber'},
                   {field:'Name'},
                   {field:'Date_of_Birth', displayName:'Date of Birth'},
                   {field:'Country'},
                   {field:'Email'},
                   {field:'Contact_No', displayName:'Contact No.'}
               ],
               headerRowTemplate: '../headerRowTemplate.html',
               footerTemplate: '../footerTemplate.html'
           };

           $scope.gridOptions4 = {
             data: 'myData',
             jqueryUITheme: true,
             enablePinning: false,
             enablePaging: true,
             showFooter: true,
             headerRowHeight: 40,
             totalServerItems: 'totalServerItems',
             pagingOptions: {
                 pageSizes: [15, 30, 60],
                 pageSize: 15,
                 totalServerItems: 0,
                 currentPage: 1
             },
             //filterOptions: $scope.filterOptions,
             columnDefs: [{
                 field: 'Date',
                 width: 200,
             }, {
                 field: 'Reference'
             }, {
                 field: 'Amount',
                 width: 150,
             }],
             headerRowTemplate: '../headerRowTemplate.html',
             showFooter: false
                 // showSelectionCheckbox: true,
                 // checkboxCellTemplate: '<div class="ngSelectionCell"><input tabindex="-1" class="ngSelectionCheckbox" type="checkbox" ng-checked="row.selected" /> Some text here</div>',
         };
       }

   ]);